class User {
  String id;
  String username;
  String firstName;
  String lastName;
  String token;
  List<String> roles;

  User(
      {this.id,
      this.username,
      this.firstName,
      this.lastName,
      this.token,
      this.roles});

  User.noRole(
      {this.id, this.username, this.firstName, this.lastName, this.token});

  User.fromJSON(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['token'] = this.token;
    return data;
  }
}

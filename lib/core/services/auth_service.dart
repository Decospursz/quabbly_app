import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jaguar_jwt/jaguar_jwt.dart';
import 'package:quabbly_app/utils/base_service.dart';
import 'package:http/http.dart' as http;
import 'package:quabbly_app/core/models/user.dart';

import 'payloads.dart';

class AuthService {
  static String baseURL = BaseService.baseURL;
  var client = new http.Client();
  final storage = new FlutterSecureStorage();

  Future<User> loginUser(username, password) async {
    return client.post('https://achara.quabbly.com/v1/auth/login',
        body: json.encode({
          'username': username,
          'password': password,
        }),
        headers: {
          "Accept": "application/json",
          "content-type": "application/json"
        }).then((response) {
      print(json.decode(response.body));
      print(json.decode(response.body)['token']);
      print(json.decode(response.body)['user']);

      //TODO decrypt token to get JWT claims.
      //save token
      saveTokenInSecureStorage(json.decode(response.body)['token']);

      // if response == 200 , proceed else return error
      //register you storage class as a global locator so that you can use it globally
      return User.fromJSON(json.decode(response.body));
    });
  }

  void saveTokenInSecureStorage(token) async {
    await storage.write(key: 'token', value: token);
  }

  Future<String> getTokenFromSecureStorage() async {
    String token = await storage.read(key: 'token');
    print(token);
    return token;
  }
}

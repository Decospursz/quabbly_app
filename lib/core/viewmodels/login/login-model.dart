import 'package:quabbly_app/core/enums/viewstate.dart';
import 'package:quabbly_app/core/models/user.dart';
import 'package:quabbly_app/core/services/auth_service.dart';
import 'package:quabbly_app/core/viewmodels/base_model.dart';
import 'package:quabbly_app/utils/locator.dart';

class LoginModel extends BaseModel {
  AuthService _authService = locator<AuthService>();

  Future<bool> loginUser(username, password)  async  {
    setState(ViewState.Busy);
    await _authService.loginUser(username, password);
    setState(ViewState.Idle);
    return true;
  }
}
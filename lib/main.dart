import 'package:flutter/material.dart';
import 'package:quabbly_app/ui/screens/accounting/accounting_screen.dart';
import 'package:quabbly_app/ui/screens/email/compose-mail_screen.dart';
import 'package:quabbly_app/ui/screens/email/draft_screen.dart';
import 'package:quabbly_app/ui/screens/file-manager/file-manager_screen.dart';
import 'package:quabbly_app/ui/screens/hr/hr_screen.dart';
import 'package:quabbly_app/ui/screens/email/inbox_screen.dart';
import 'package:quabbly_app/ui/screens/inventory/inventory_screen.dart';
import 'package:quabbly_app/ui/screens/email/mail-content_screen.dart';
import 'package:quabbly_app/ui/screens/email/mail-spam_screen.dart';
import 'package:quabbly_app/ui/screens/email/mail-trash_screen.dart';
import 'package:quabbly_app/ui/screens/email/outbox_screen.dart';
import 'package:quabbly_app/ui/screens/onboarding/splash_screen.dart';
import 'package:quabbly_app/ui/screens/task-manager/task-manager_screen.dart';
import 'package:quabbly_app/ui/screens/task-manager/task-page_screen.dart';

import 'utils/locator.dart';

void main() {
  setupLocator();
  runApp(QuabblyApp());
}

class QuabblyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      routes: {
        '/inventory': (context) => Inventory(),
        '/task-manager': (context) => TaskManager(),
        '/file-manager': (context) => FileManager(),
        '/hr': (context) => HR(),
        '/accounting': (context) => Accounting(),
        '/mailbox': (context) => Inbox(),
        '/compose-mail': (context) => ComposeMail(),
        '/mail-content': (context) => MailContent(),
        '/drafts': (context) => DraftScreen(),
        '/outbox': (context) => OutboxScreen(),
        '/spam': (context) => Spam(),
        '/trash': (context) => TrashScreen(),
        '/task-page': (context) => TaskPage(),
      },
    );
  }
}

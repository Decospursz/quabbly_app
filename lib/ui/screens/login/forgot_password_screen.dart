import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'login_screen.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF004BF4),
      body: SafeArea(
        child: Container(
          margin: EdgeInsets.only(top: 30.0, left: 20.0),
          child: ListView(
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return LoginScreen();
                    }),
                  );
                },
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.chevron_left,
                      color: Colors.white,
                    ),
                    Text(
                      'Back',
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Open Sans',
                          fontSize: 17.0),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10.0),
              Row(
                children: <Widget>[
                  Text(
                    'Forgot password',
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Open Sans',
                        fontWeight: FontWeight.bold,
                        fontSize: 25.0),
                  )
                ],
              ),
              SizedBox(height: 10.0),
              Row(
                children: <Widget>[
                  Text(
                    'Please type your email.',
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Open Sans',
                        fontSize: 15.0),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    'An email will be sent to reset your password',
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Open Sans',
                        fontSize: 15.0),
                  )
                ],
              ),
              SizedBox(height: 50.0),
              SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 40.0),
                  child: Column(
                    children: <Widget>[
                      TextField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          hintText: 'Your email',
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontFamily: 'Open Sans',
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 35.0),
                      child: Container(
                        width: 170.0,
                        height: 40.0,
                        margin: EdgeInsets.only(top: 20.0),
                        child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            color: Colors.white,
                            textColor: Color(0xFF004BF4),
                            onPressed: () {
                              Alert(
                                style: AlertStyle(
                                    overlayColor:
                                        Colors.black.withOpacity(0.3)),
                                context: context,
                                title: "Verified!",
                                desc:
                                    "A verification link has been sent to your email.",
                                buttons: [
                                  DialogButton(
                                    child: Icon(
                                      Icons.check,
                                      color: Colors.white,
                                      size: 60,
                                    ),
                                    onPressed: () => Navigator.pop(context),
                                    color: Colors.green[600],
                                    radius: BorderRadius.circular(100.0),
                                    width: 70,
                                    height: 70,
                                  ),
                                ],
                              ).show();
                            },
                            child: Text(
                              'SUBMIT',
                              style: TextStyle(
                                  fontFamily: 'Open Sans', fontSize: 16),
                            )),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

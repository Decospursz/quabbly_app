import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quabbly_app/core/enums/viewstate.dart';
import 'package:quabbly_app/core/models/user.dart';
import 'package:quabbly_app/core/services/auth_service.dart';
import 'package:quabbly_app/core/services/payloads.dart';
import 'package:quabbly_app/core/viewmodels/login/login-model.dart';
import 'package:quabbly_app/utils/locator.dart';

import '../dashboard_screen.dart';
import 'forgot_password_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  AuthService authService = new AuthService();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LoginModel>(
        builder: (context) => locator<LoginModel>(),
        child: Consumer<LoginModel>(
          builder: (context, model, child) => Scaffold(
            backgroundColor: Colors.white,
            body: SafeArea(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 0,
                    child: ClipPath(
                        clipper: LinePathClass(),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              height: MediaQuery.of(context).size.height * 0.5,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('images/login.png'),
                                    fit: BoxFit.fitWidth),
                              ),
                            ),
                            Container(
                              height: MediaQuery.of(context).size.height * 0.5,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.4)),
                            ),
                          ],
                        )),
                  ),
                  Positioned(
                    bottom: 0,
                    child: RotatedBox(
                      quarterTurns: 2,
                      child: ClipPath(
                        clipper: LinePathClass(),
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.51,
                          width: MediaQuery.of(context).size.width,
                          color: Color(0xFF004BF4),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(8.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12,
                                  offset: Offset(0.0, 15.0),
                                  blurRadius: 15.0,
                                ),
                                BoxShadow(
                                  color: Colors.black12,
                                  offset: Offset(0.0, -10.0),
                                  blurRadius: 10.0,
                                )
                              ]),
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: 16.0, right: 16.0, top: 40.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Email',
                                  style: TextStyle(
                                    fontFamily: 'OpenSans',
                                  ),
                                ),
                                TextField(
                                  controller: emailController,
                                  onChanged: (email) {},
                                  decoration: InputDecoration(
                                      hintText: 'email',
                                      hintStyle: TextStyle(
                                        color: Color(0xFF77869E),
                                        fontSize: 12.0,
                                        fontFamily: 'OpenSans',
                                      )),
                                ),
                                SizedBox(height: 15),
                                Text(
                                  'Password',
                                  style: TextStyle(
                                    fontFamily: 'OpenSans',
                                  ),
                                ),
                                TextField(
                                  controller: passwordController,
                                  onChanged: (password) {},
                                  obscureText: true,
                                  decoration: InputDecoration(
                                      hintText: 'password',
                                      hintStyle: TextStyle(
                                        color: Color(0xFF77869E),
                                        fontSize: 12.0,
                                        fontFamily: 'OpenSans',
                                      )),
                                ),
                                SizedBox(height: 15.0),
                                GestureDetector(
                                  child: Text(
                                    'Forgot password?',
                                    style: TextStyle(
                                      color: Color(0xFF004BF4),
                                      fontWeight: FontWeight.w600,
                                      fontFamily: 'OpenSans',
                                      fontSize: 14.0,
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) {
                                          return ForgotPasswordScreen();
                                        }),
                                      );
                                    });
                                  },
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(vertical: 16.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 35.0),
                                        child: Container(
                                          width: 170.0,
                                          height: 40.0,
                                          margin: EdgeInsets.only(top: 20.0),
                                          child: RaisedButton(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      30.0),
                                            ),
                                            color: Color(0xFF004BF4),
                                            textColor: Colors.white,
                                            onPressed: () async {
                                              print(model.state);
                                              await model.loginUser(
                                                  '${emailController.text}',
                                                  '${passwordController.text}');

                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) {
                                                  return DashboardScreen();
                                                }),
                                              );
                                            },
                                            child: model.state == ViewState.Busy
                                                ? Container(
                                                      height: 16,
                                                    width: 16,
                                                    child:
                                                        CircularProgressIndicator(
                                                            backgroundColor:
                                                                Colors.white),
                                                  )
                                                : Text(
                                                    'LOGIN',
                                                    style: TextStyle(
                                                      fontFamily: 'OpenSans',
                                                    ),
                                                  ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}

class LinePathClass extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path()
      ..lineTo(0.0, size.height - (size.height * 0.5))
      ..lineTo(size.width, size.height)
      ..lineTo(size.width, 0.0)
      ..close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return false;
  }
}

import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class TaskPage extends StatefulWidget {
  @override
  _TaskPageState createState() => _TaskPageState();
}

class _TaskPageState extends State<TaskPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Row(
            children: <Widget>[
              Icon(
                Icons.chevron_left,
                color: Color(0xff00174A),
                size: 25,
              ),
              Text(
                'Back',
                style: TextStyle(
                  color: Color(0xff00174A),
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: '[UX] [Mobile app] (1)  ',
                          style: TextStyle(
                            fontSize: 22,
                            color: Color(0xff00174A),
                            fontWeight: FontWeight.w400,
                            fontFamily: 'OpenSans-Semi-Bold',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundColor: Color(0xff37C5AB),
              radius: 22,
              child: Text(
                'SU',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            title: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Assigned to  ',
                    style: TextStyle(
                      color: Color(0xff828282),
                      fontFamily: 'OpenSans-Bold',
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ),
            subtitle: Text(
              'supremeux@gmail.com  ',
              style: TextStyle(
                color: Color(0xff00174A),
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Text('Task Description'),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 16,
                  width: 16,
                  decoration: BoxDecoration(
                    color: Color(0xff4186E0),
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.05,
                ),
                Text('Project Name'),
              ],
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text('Column'),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.05,
                ),
                InkWell(
                  onTap: () {
                    _modalBottomSheetMenu();
                  },
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Filter',
                          style: TextStyle(
                            color: Color(0xff4F4F4F),
                          ),
                        ),
                        Icon(
                          Icons.keyboard_arrow_down,
                          color: Color(0xff4F4F4F),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
                    decoration: BoxDecoration(
                      color: Color(0xffeeeeee),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _modalBottomSheetMenu() {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      backgroundColor: Colors.white,
      context: context,
      builder: (builder) {
        return new Container(
          height: MediaQuery.of(context).size.height * 0.38,
          color: Colors.transparent,
          child: Padding(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.034,
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.08),
                  child: Text(
                    'Column',
                    style: TextStyle(
                      color: Color(0xff00174A),
                      fontWeight: FontWeight.w600,
                      fontFamily: 'OpenSans',
                    ),
                  ),
                ),
                new RadioButtonGroup(
                  padding: EdgeInsets.all(0),
                  labels: <String>[
                    "TODO",
                    "IN PROGRESS",
                    "DONE",
                  ],
                  onSelected: (String selected) => print(selected),
                  activeColor: Color(0xff004BF4),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

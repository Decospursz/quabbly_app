import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class TaskManager extends StatefulWidget {
  @override
  _TaskManagerState createState() => _TaskManagerState();
}

class _TaskManagerState extends State<TaskManager> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _hideParentApps = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(0xffe5e5e5),
      appBar: AppBar(
        title: Text(
          'Task Manager',
          style: TextStyle(
            color: Color(0xff00174A),
          ),
        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: InkWell(
          child: Image.asset('images/menu.png'),
          onTap: () {
            _scaffoldKey.currentState.openDrawer();
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.grey),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.notifications_none, color: Colors.grey),
            onPressed: () {},
          )
        ],
      ),
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Color(0xFF004BF4),
        ),
        child: Drawer(
          child: _hideParentApps
              ? new ListView(
                  children: <Widget>[
                    ListTile(
                      trailing: InkWell(
                        child: Icon(
                          Icons.apps,
                          size: 25,
                          color: Colors.white.withOpacity(0.75),
                        ),
                        onTap: () {
                          setState(() => _hideParentApps = false);
                        },
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    ListTile(
                      dense: true,
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 0,
                        horizontal: 8,
                      ),
                      leading: Text(
                        'PROJECTS',
                        style: TextStyle(
                          color: Colors.white.withOpacity(0.8),
                          fontSize: 12,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      trailing: Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5),
                          ),
                          color: Colors.white.withOpacity(0.1),
                        ),
                        child: Text(
                          'Number of tasks left',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 13,
                            fontFamily: 'OpenSans',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
                    ListTile(
                      onTap: () {},
                      dense: true,
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 0,
                        horizontal: 8,
                      ),
                      leading: Text(
                        'Front End Development ',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      trailing: Container(
                        height: 20,
                        width: 30,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.white,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Center(
                          child: Text(
                            '12',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w300,
                                fontSize: 12),
                          ),
                        ),
                      ),
                    ),
                    ListTile(
                      onTap: () {},
                      dense: true,
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 0,
                        horizontal: 8,
                      ),
                      leading: Text(
                        'Task Manager Mockups ',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      trailing: Container(
                        height: 20,
                        width: 30,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.white,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Center(
                          child: Text(
                            '12',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w300,
                                fontSize: 12),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : ListView(
                  children: <Widget>[
                    ListTile(
                      trailing: InkWell(
                        child: Icon(
                          Icons.close,
                          size: 25,
                          color: Colors.white.withOpacity(0.75),
                        ),
                        onTap: () {
                          setState(() => _hideParentApps = true);
                        },
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    ListTile(
                      onTap: () {
                        _hideParentApps = true;
                        Navigator.pop(context);
                        Navigator.of(context).pushNamed('/task-manager');
                      },
                      dense: true,
                      title: Text(
                        'Task Manager',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      subtitle: Text(
                        'Manage projects for your organisation',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w200,
                            fontSize: 12),
                      ),
                      leading: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Color(0xff0036B0),
                        ),
                        child: Image.asset('images/clipboard.png'),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        _hideParentApps = true;
                        Navigator.pop(context);
                        Navigator.of(context).pushNamed('/file-manager');
                      },
                      dense: true,
                      title: Text(
                        'File Manager',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      subtitle: Text(
                        'Manage yout office files with ease',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w200,
                            fontSize: 12),
                      ),
                      leading: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Color(0xff0036B0),
                        ),
                        child: Image.asset('images/copy.png'),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        _hideParentApps = true;
                        Navigator.pop(context);
                        Navigator.of(context).pushNamed('/mailbox');
                      },
                      dense: true,
                      title: Text(
                        'Mailbox',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      subtitle: Text(
                        'Your mailbox with your domain name',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w200,
                            fontSize: 12),
                      ),
                      leading: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Color(0xff0036B0),
                        ),
                        child: Image.asset('images/clipboard.png'),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        _hideParentApps = true;
                        Navigator.pop(context);
                        Navigator.of(context).pushNamed('/hr');
                      },
                      dense: true,
                      title: Text(
                        'Human Resource Manager',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      subtitle: Text(
                        'Manage your team efficiently',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w200,
                            fontSize: 12),
                      ),
                      leading: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Color(0xff0036B0),
                        ),
                        child: Image.asset('images/teamwork.png'),
                      ),
                    ),
                    ListTile(
                      onTap: () {
                        _hideParentApps = true;
                        Navigator.pop(context);
                        Navigator.of(context).pushNamed('/accounting');
                      },
                      dense: true,
                      title: Text(
                        'Accounting',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      subtitle: Text(
                        'Manage your business finance',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w200,
                            fontSize: 12),
                      ),
                      leading: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Color(0xff0036B0),
                        ),
                        child: Image.asset('images/budget.png'),
                      ),
                    ),
                    ListTile(
                      dense: true,
                      onTap: () {
                        _hideParentApps = true;
                        Navigator.pop(context);
                        Navigator.of(context).pushNamed('/inventory');
                      },
                      title: Text(
                        'Inventory',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      subtitle: Text(
                        'Manage your products and inventory',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w200,
                            fontSize: 12),
                      ),
                      leading: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Color(0xff0036B0),
                        ),
                        child: Image.asset('images/boxes.png'),
                      ),
                    ),
                  ],
                ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    _modalBottomSheetMenu();
                  },
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Filter',
                          style: TextStyle(
                            color: Color(0xff4F4F4F),
                          ),
                        ),
                        Icon(
                          Icons.keyboard_arrow_down,
                          color: Color(0xff4F4F4F),
                        ),
                      ],
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
                    decoration: BoxDecoration(
                      color: Color(0xffeeeeee),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              elevation: 0,
              child: ListTile(
                onTap: () {
                  Navigator.of(context).pushNamed('/task-page');
                },
                leading: CircleAvatar(
                  backgroundColor: Color(0xff37C5AB),
                  radius: 15,
                ),
                isThreeLine: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0, bottom: 5.0),
                      child: Text(
                        'Website Creator',
                        style: TextStyle(
                          color: Color(0xff00174A),
                          fontSize: 14,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        'Created',
                        style: TextStyle(
                          color: Color(0xffbdbdbd),
                          fontSize: 14,
                          fontFamily: 'OpenSans-Semi-Bold',
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Oct 10 2019, 7:01 AM',
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontSize: 12,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.w100,
                  ),
                ),
                trailing: Container(
                  height: 24,
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Color(0xffFCE6CA),
                  ),
                  child: Center(
                    child: Text(
                      'IN PROGRESS',
                      style: TextStyle(
                        color: Color(0xffCD780C),
                        fontSize: 10,
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              elevation: 0,
              child: ListTile(
                onTap: () {},
                leading: CircleAvatar(
                  backgroundColor: Color(0xff37C5AB),
                  radius: 15,
                ),
                isThreeLine: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0, bottom: 5.0),
                      child: Text(
                        'Website Creator',
                        style: TextStyle(
                          color: Color(0xff00174A),
                          fontSize: 14,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        'Created',
                        style: TextStyle(
                          color: Color(0xffbdbdbd),
                          fontSize: 14,
                          fontFamily: 'OpenSans-Semi-Bold',
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Oct 10 2019, 7:01 AM',
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontSize: 12,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.w100,
                  ),
                ),
                trailing: Container(
                  height: 24,
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Color(0xffCDF8FE),
                  ),
                  child: Center(
                    child: Text(
                      'TODO',
                      style: TextStyle(
                        color: Color(0xff04B0C8),
                        fontSize: 10,
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              elevation: 0,
              child: ListTile(
                onTap: () {},
                leading: CircleAvatar(
                  backgroundColor: Color(0xff37C5AB),
                  radius: 15,
                ),
                isThreeLine: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0, bottom: 5.0),
                      child: Text(
                        'Website Creator',
                        style: TextStyle(
                          color: Color(0xff00174A),
                          fontSize: 14,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        'Created',
                        style: TextStyle(
                          color: Color(0xffbdbdbd),
                          fontSize: 14,
                          fontFamily: 'OpenSans-Semi-Bold',
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Oct 10 2019, 7:01 AM',
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontSize: 12,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.w100,
                  ),
                ),
                trailing: Container(
                  height: 24,
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Color(0xffFCE6CA),
                  ),
                  child: Center(
                    child: Text(
                      'IN PROGRESS',
                      style: TextStyle(
                        color: Color(0xffCD780C),
                        fontSize: 10,
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              elevation: 0,
              child: ListTile(
                onTap: () {},
                leading: CircleAvatar(
                  backgroundColor: Color(0xff37C5AB),
                  radius: 15,
                ),
                isThreeLine: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0, bottom: 5.0),
                      child: Text(
                        'Website Creator',
                        style: TextStyle(
                          color: Color(0xff00174A),
                          fontSize: 14,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        'Created',
                        style: TextStyle(
                          color: Color(0xffbdbdbd),
                          fontSize: 14,
                          fontFamily: 'OpenSans-Semi-Bold',
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Oct 10 2019, 7:01 AM',
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontSize: 12,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.w100,
                  ),
                ),
                trailing: Container(
                  height: 24,
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Color(0xffCFF7EE),
                  ),
                  child: Center(
                    child: Text(
                      'DONE',
                      style: TextStyle(
                        color: Color(0xff21C8A2),
                        fontSize: 10,
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              elevation: 0,
              child: ListTile(
                onTap: () {},
                leading: CircleAvatar(
                  backgroundColor: Color(0xff37C5AB),
                  radius: 15,
                ),
                isThreeLine: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0, bottom: 5.0),
                      child: Text(
                        'Website Creator',
                        style: TextStyle(
                          color: Color(0xff00174A),
                          fontSize: 14,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        'Created',
                        style: TextStyle(
                          color: Color(0xffbdbdbd),
                          fontSize: 14,
                          fontFamily: 'OpenSans-Semi-Bold',
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Oct 10 2019, 7:01 AM',
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontSize: 12,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.w100,
                  ),
                ),
                trailing: Container(
                  height: 24,
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Color(0xffCFF7EE),
                  ),
                  child: Center(
                    child: Text(
                      'DONE',
                      style: TextStyle(
                        color: Color(0xff21C8A2),
                        fontSize: 10,
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              elevation: 0,
              child: ListTile(
                onTap: () {},
                leading: CircleAvatar(
                  backgroundColor: Color(0xff37C5AB),
                  radius: 15,
                ),
                isThreeLine: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0, bottom: 5.0),
                      child: Text(
                        'Website Creator',
                        style: TextStyle(
                          color: Color(0xff00174A),
                          fontSize: 14,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        'Created',
                        style: TextStyle(
                          color: Color(0xffbdbdbd),
                          fontSize: 14,
                          fontFamily: 'OpenSans-Semi-Bold',
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Oct 10 2019, 7:01 AM',
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontSize: 12,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.w100,
                  ),
                ),
                trailing: Container(
                  height: 24,
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Color(0xffFCE6CA),
                  ),
                  child: Center(
                    child: Text(
                      'IN PROGRESS',
                      style: TextStyle(
                        color: Color(0xffCD780C),
                        fontSize: 10,
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              elevation: 0,
              child: ListTile(
                onTap: () {},
                leading: CircleAvatar(
                  backgroundColor: Color(0xff37C5AB),
                  radius: 15,
                ),
                isThreeLine: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0, bottom: 5.0),
                      child: Text(
                        'Website Creator',
                        style: TextStyle(
                          color: Color(0xff00174A),
                          fontSize: 14,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        'Created',
                        style: TextStyle(
                          color: Color(0xffbdbdbd),
                          fontSize: 14,
                          fontFamily: 'OpenSans-Semi-Bold',
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Oct 10 2019, 7:01 AM',
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontSize: 12,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.w100,
                  ),
                ),
                trailing: Container(
                  height: 24,
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Color(0xffCDF8FE),
                  ),
                  child: Center(
                    child: Text(
                      'TODO',
                      style: TextStyle(
                        color: Color(0xff04B0C8),
                        fontSize: 10,
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              elevation: 0,
              child: ListTile(
                onTap: () {},
                leading: CircleAvatar(
                  backgroundColor: Color(0xff37C5AB),
                  radius: 15,
                ),
                isThreeLine: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0, bottom: 5.0),
                      child: Text(
                        'Website Creator',
                        style: TextStyle(
                          color: Color(0xff00174A),
                          fontSize: 14,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        'Created',
                        style: TextStyle(
                          color: Color(0xffbdbdbd),
                          fontSize: 14,
                          fontFamily: 'OpenSans-Semi-Bold',
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Oct 10 2019, 7:01 AM',
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontSize: 12,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.w100,
                  ),
                ),
                trailing: Container(
                  height: 24,
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Color(0xffFCE6CA),
                  ),
                  child: Center(
                    child: Text(
                      'IN PROGRESS',
                      style: TextStyle(
                        color: Color(0xffCD780C),
                        fontSize: 10,
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              elevation: 0,
              child: ListTile(
                onTap: () {},
                leading: CircleAvatar(
                  backgroundColor: Color(0xff37C5AB),
                  radius: 15,
                ),
                isThreeLine: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0, bottom: 5.0),
                      child: Text(
                        'Website Creator',
                        style: TextStyle(
                          color: Color(0xff00174A),
                          fontSize: 14,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        'Created',
                        style: TextStyle(
                          color: Color(0xffbdbdbd),
                          fontSize: 14,
                          fontFamily: 'OpenSans-Semi-Bold',
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Oct 10 2019, 7:01 AM',
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontSize: 12,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.w100,
                  ),
                ),
                trailing: Container(
                  height: 24,
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Color(0xffCDF8FE),
                  ),
                  child: Center(
                    child: Text(
                      'TODO',
                      style: TextStyle(
                        color: Color(0xff04B0C8),
                        fontSize: 10,
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Card(
              elevation: 0,
              child: ListTile(
                onTap: () {},
                leading: CircleAvatar(
                  backgroundColor: Color(0xff37C5AB),
                  radius: 15,
                ),
                isThreeLine: true,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0, bottom: 5.0),
                      child: Text(
                        'Website Creator',
                        style: TextStyle(
                          color: Color(0xff00174A),
                          fontSize: 14,
                          fontFamily: 'OpenSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5.0),
                      child: Text(
                        'Created',
                        style: TextStyle(
                          color: Color(0xffbdbdbd),
                          fontSize: 14,
                          fontFamily: 'OpenSans-Semi-Bold',
                          fontWeight: FontWeight.w100,
                        ),
                      ),
                    ),
                  ],
                ),
                subtitle: Text(
                  'Oct 10 2019, 7:01 AM',
                  style: TextStyle(
                    color: Color(0xff828282),
                    fontSize: 12,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.w100,
                  ),
                ),
                trailing: Container(
                  height: 24,
                  width: 96,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                    color: Color(0xffFCE6CA),
                  ),
                  child: Center(
                    child: Text(
                      'IN PROGRESS',
                      style: TextStyle(
                        color: Color(0xffCD780C),
                        fontSize: 10,
                        fontFamily: 'OpenSans',
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _modalBottomSheetMenu() {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      backgroundColor: Colors.white,
      context: context,
      builder: (builder) {
        return new Container(
          height: MediaQuery.of(context).size.height * 0.38,
          color: Colors.transparent,
          child: Padding(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.034,
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.08),
                  child: Text(
                    'Column',
                    style: TextStyle(
                      color: Color(0xff00174A),
                      fontWeight: FontWeight.w600,
                      fontFamily: 'OpenSans',
                    ),
                  ),
                ),
                new RadioButtonGroup(
                  padding: EdgeInsets.all(0),
                  labels: <String>[
                    "TODO",
                    "IN PROGRESS",
                    "DONE",
                  ],
                  onSelected: (String selected) => print(selected),
                  activeColor: Color(0xff004BF4),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

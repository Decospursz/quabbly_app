import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:quabbly_app/core/services/auth_service.dart';
import 'package:quabbly_app/utils/locator.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool show = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(0xFFf2f2f4),
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.apps,
            color: Color(0xFF004BF4),
          ),
          onPressed: () {
            _scaffoldKey.currentState.openDrawer();
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.grey),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.notifications_none, color: Colors.grey),
            onPressed: () {},
          )
        ],
      ),
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Color(0xFF004BF4),
        ),
        child: Drawer(
          child: new ListView(
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/task-manager');
                },
                dense: true,
                title: Text(
                  'Task Manager',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Manage projects for your organisation',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/clipboard.png'),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/file-manager');
                },
                dense: true,
                title: Text(
                  'File Manager',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Manage yout office files with ease',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/copy.png'),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/mailbox');
                },
                dense: true,
                title: Text(
                  'Mailbox',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Your mailbox with your domain name',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/clipboard.png'),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/hr');
                },
                dense: true,
                title: Text(
                  'Human Resource Manager',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Manage your team efficiently',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/teamwork.png'),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/accounting');
                },
                dense: true,
                title: Text(
                  'Accounting',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Manage your business finance',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/budget.png'),
                ),
              ),
              ListTile(
                dense: true,
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/inventory');
                },
                title: Text(
                  'Inventory',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Manage your products and inventory',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/boxes.png'),
                ),
              ),
            ],
          ),
        ),
      ),
      body: GridView.count(
        crossAxisCount: 2,
        children: List.generate(dashboardItems.length, (index) {
          return Center(
            child: DashboardCard(
              dashboardItem: dashboardItems[index],
            ),
          );
        }),
      ),
    );
  }
}

class DashboardCard extends StatelessWidget {
  const DashboardCard({Key key, this.dashboardItem}) : super(key: key);
  final DashboardItem dashboardItem;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        AuthService authService = locator<AuthService>();
        String token = await authService.getTokenFromSecureStorage();
        print(token);
        Navigator.of(context).pushNamed(dashboardItem.routeName);
      },
      child: Card(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image(
                image: AssetImage(dashboardItem.imageUrl),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text(
                  dashboardItem.title,
                  style: TextStyle(
                      fontFamily: 'OpenSans',
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Color(0xff00174A)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

const List<DashboardItem> dashboardItems = const <DashboardItem>[
  const DashboardItem(
      title: 'Task Management',
      imageUrl: 'images/taskmanager.png',
      routeName: '/task-manager'),
  const DashboardItem(
      title: 'File Management',
      imageUrl: 'images/filemanager.png',
      routeName: '/file-manager'),
  const DashboardItem(
      title: 'Mailbox', imageUrl: 'images/mailbox.png', routeName: '/mailbox'),
  const DashboardItem(
      title: 'HR Management', imageUrl: 'images/hr.png', routeName: '/hr'),
  const DashboardItem(
      title: 'Accounting',
      imageUrl: 'images/accounting.png',
      routeName: '/accounting'),
  const DashboardItem(
      title: 'Inventory',
      imageUrl: 'images/accounting.png',
      routeName: '/inventory'),
];

class DashboardItem {
  final String imageUrl;
  final String title;
  final String routeName;

  const DashboardItem({this.imageUrl, this.title, this.routeName});
}

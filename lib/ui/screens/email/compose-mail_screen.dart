import 'package:flutter/material.dart';

class ComposeMail extends StatefulWidget {
  @override
  _ComposeMailState createState() => _ComposeMailState();
}

class _ComposeMailState extends State<ComposeMail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        actions: <Widget>[
          InkWell(
            onTap: () {},
            child: Image.asset('images/attachment.png'),
          ),
          InkWell(
            child: Image.asset('images/send.png'),
            onTap: () {},
          ),
        ],
        backgroundColor: Colors.white,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Row(
            children: <Widget>[
              Icon(
                Icons.chevron_left,
                color: Color(0xff00174A),
                size: 25,
              ),
              Text(
                'Back',
                style: TextStyle(
                  color: Color(0xff00174A),
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              'Compose Mail',
              style: TextStyle(
                fontSize: 22,
                color: Color(0xff00174A),
                fontWeight: FontWeight.w400,
                fontFamily: 'OpenSans-Semi-Bold',
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              'To',
              style: TextStyle(
                color: Color(0xff858585),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: TextField(
              cursorColor: Color(0xff00174A),
              decoration: InputDecoration(
                suffixIcon: Image.asset('images/contact.png'),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xff858585)),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.green),
                ),
                hintText: 'Recipient Email',
                hintStyle: TextStyle(
                  color: Color(0xff858585),
                  fontSize: 16.0,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: TextField(
              cursorColor: Color(0xff00174A),
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xff858585)),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.green),
                ),
                hintText: 'cc',
                hintStyle: TextStyle(
                  color: Color(0xff858585),
                  fontSize: 16.0,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: TextField(
              cursorColor: Color(0xff00174A),
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Color(0xff858585)),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.green),
                ),
                hintText: 'Subject',
                hintStyle: TextStyle(
                  color: Color(0xff858585),
                  fontSize: 16.0,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: TextField(
              maxLines: 20,
              cursorColor: Color(0xff00174A),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Your Mail',
                hintStyle: TextStyle(
                  color: Color(0xff858585),
                  fontSize: 16.0,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

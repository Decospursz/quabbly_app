import 'package:flutter/material.dart';

class MailContent extends StatefulWidget {
  @override
  _MailContentState createState() => _MailContentState();
}

class _MailContentState extends State<MailContent> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      persistentFooterButtons: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width * 0.9,
          height: 50,
          child: Row(
            children: <Widget>[
              InkWell(
                onTap: () {},
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.25,
                  height: 40,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    border: Border.all(
                      color: Color(0xff828282),
                      width: 1,
                    ),
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Image.asset('images/reply.png'),
                          Text(
                            'Reply',
                            style: TextStyle(
                              color: Color(0xff828282),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.05,
              ),
              InkWell(
                onTap: () {},
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.25,
                  height: 40,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    border: Border.all(
                      color: Color(0xff828282),
                      width: 1,
                    ),
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 7),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Image.asset('images/replyall.png'),
                          Text(
                            'Reply All',
                            style: TextStyle(
                              color: Color(0xff828282),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.05,
              ),
              InkWell(
                onTap: () {},
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.25,
                  height: 40,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    border: Border.all(
                      color: Color(0xff828282),
                      width: 1,
                    ),
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 7.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Image.asset('images/forward.png'),
                          Text(
                            'Forward',
                            style: TextStyle(
                              color: Color(0xff828282),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        actions: <Widget>[
          Image.asset('images/delete.png'),
        ],
        backgroundColor: Colors.white,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Row(
            children: <Widget>[
              Icon(
                Icons.chevron_left,
                color: Color(0xff00174A),
                size: 25,
              ),
              Text(
                'Back',
                style: TextStyle(
                  color: Color(0xff00174A),
                  fontWeight: FontWeight.w500,
                ),
              ),
            ],
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              children: <Widget>[
                Flexible(
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text:
                              'Assign to you: [UX] [Mobile app] (1) [Quabbly Onboarding Sprint]  ',
                          style: TextStyle(
                            fontSize: 22,
                            color: Color(0xff00174A),
                            fontWeight: FontWeight.w400,
                            fontFamily: 'OpenSans-Semi-Bold',
                          ),
                        ),
                        TextSpan(
                          text: 'Inbox',
                          style: TextStyle(
                            backgroundColor: Color(0xffeeeeee),
                            color: Color(0xff4F4F4F),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Icon(
                  Icons.star_border,
                  color: Color(0xff00174A),
                  size: 19,
                )
              ],
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          ListTile(
            leading: CircleAvatar(
              backgroundColor: Color(0xff9CCC65),
              radius: 22,
              child: Text(
                'R',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            title: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'Ralph via Asana  ',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'OpenSans-Bold',
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                    ),
                  ),
                  TextSpan(
                    text: '7:12',
                    style: TextStyle(
                      color: Color(0xff828282),
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            subtitle: Row(
              children: <Widget>[
                Text(
                  'to me  ',
                  style: TextStyle(
                    color: Color(0xff828282),
                  ),
                ),
                Icon(
                  Icons.expand_more,
                  color: Color(0xff828282),
                  size: 15,
                )
              ],
            ),
            trailing: Image.asset('images/sender.png'),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Text('Email Content'),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

class Spam extends StatefulWidget {
  @override
  _SpamState createState() => _SpamState();
}

class _SpamState extends State<Spam> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _hideParentApps = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
//        title: Text(
//          'inbox',
//          style: TextStyle(
//            color: Colors.black,
//          ),
//        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: InkWell(
          child: Image.asset('images/menu.png'),
          onTap: () {
            _scaffoldKey.currentState.openDrawer();
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.grey),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.notifications_none, color: Colors.grey),
            onPressed: () {},
          )
        ],
      ),
      drawer: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Color(0xFF004BF4),
        ),
        child: Drawer(
          child: _hideParentApps
              ? new ListView(
            children: <Widget>[
              ListTile(
                trailing: InkWell(
                  child: Icon(
                    Icons.apps,
                    size: 25,
                    color: Colors.white.withOpacity(0.75),
                  ),
                  onTap: () {
                    setState(() => _hideParentApps = false);
                  },
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/mailbox');
                },
                dense: true,
                title: Text(
                  'Inbox',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  child: Icon(
                    Icons.mail,
                    color: Colors.white.withOpacity(0.75),
                    size: 18,
                  ),
                ),
                trailing: Container(
                  height: 20,
                  width: 30,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Center(
                    child: Text(
                      '12',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                          fontSize: 12),
                    ),
                  ),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/outbox');
                },
                dense: true,
                title: Text(
                  'Outbox',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                leading: Container(
                    width: 40,
                    height: 40,
                    child: Image.asset('images/outbox.png')),
                trailing: Container(
                  height: 20,
                  width: 30,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Center(
                    child: Text(
                      '12',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                          fontSize: 12),
                    ),
                  ),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/drafts');
                },
                dense: true,
                title: Text(
                  'Drafts',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  child: Icon(
                    Icons.drafts,
                    color: Colors.white.withOpacity(0.75),
                    size: 18,
                  ),
                ),
                trailing: Container(
                  height: 20,
                  width: 30,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Center(
                    child: Text(
                      '12',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                          fontSize: 12),
                    ),
                  ),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/spam');
                },
                dense: true,
                title: Text(
                  'Spam',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                leading: Container(
                    width: 40,
                    height: 40,
                    child: Image.asset('images/spam.png')),
                trailing: Container(
                  height: 20,
                  width: 30,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Center(
                    child: Text(
                      '12',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                          fontSize: 12),
                    ),
                  ),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/trash');
                },
                dense: true,
                title: Text(
                  'Trash',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                leading: Container(
                    width: 40,
                    height: 40,
                    child: Image.asset('images/trash.png')),
                trailing: Container(
                  height: 20,
                  width: 30,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.white,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Center(
                    child: Text(
                      '12',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                          fontSize: 12),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Container(
                height: 30,
                child: ListTile(
                  dense: true,
                  leading: Text(
                    'OTHERS',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 11,
                    ),
                  ),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.pop(context);
                  print('hello');
                },
                dense: true,
                leading: Text(
                  'Newsletters',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                  ),
                ),
              ),
            ],
          )
              : ListView(
            children: <Widget>[
              ListTile(
                trailing: InkWell(
                  child: Icon(
                    Icons.close,
                    size: 25,
                    color: Colors.white.withOpacity(0.75),
                  ),
                  onTap: () {
                    setState(() => _hideParentApps = true);
                  },
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.02,
              ),
              ListTile(
                onTap: () {
                  _hideParentApps = true;
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/task-manager');
                },
                dense: true,
                title: Text(
                  'Task Manager',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Manage projects for your organisation',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/clipboard.png'),
                ),
              ),
              ListTile(
                onTap: () {
                  _hideParentApps = true;
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/file-manager');
                },
                dense: true,
                title: Text(
                  'File Manager',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Manage yout office files with ease',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/copy.png'),
                ),
              ),
              ListTile(
                onTap: () {
                  _hideParentApps = true;
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/mailbox');
                },
                dense: true,
                title: Text(
                  'Mailbox',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Your mailbox with your domain name',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/clipboard.png'),
                ),
              ),
              ListTile(
                onTap: () {
                  _hideParentApps = true;
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/hr');
                },
                dense: true,
                title: Text(
                  'Human Resource Manager',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Manage your team efficiently',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/teamwork.png'),
                ),
              ),
              ListTile(
                onTap: () {
                  _hideParentApps = true;
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/accounting');
                },
                dense: true,
                title: Text(
                  'Accounting',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Manage your business finance',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/budget.png'),
                ),
              ),
              ListTile(
                dense: true,
                onTap: () {
                  _hideParentApps = true;
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed('/inventory');
                },
                title: Text(
                  'Inventory',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                subtitle: Text(
                  'Manage your products and inventory',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w200,
                      fontSize: 12),
                ),
                leading: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Color(0xff0036B0),
                  ),
                  child: Image.asset('images/boxes.png'),
                ),
              ),
            ],
          ),
        ),
      ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color(0xFF004BF4),
          onPressed: () {
            Navigator.of(context).pushNamed('/compose-mail');
          },
          tooltip: 'Compose',
          child: Icon(
            Icons.add,
            size: 25,
          ),
        ),
        body: InboxCard());
  }
}

class InboxCard extends StatelessWidget {
  const InboxCard({Key key, this.inboxItem}) : super(key: key);
  final InboxItem inboxItem;
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 8.0),
              child: Text(
                'SPAM',
                style: TextStyle(
                    fontSize: 12,
                    color: Color(0xff00174A),
                    fontWeight: FontWeight.w500),
              ),
            )
          ],
        ),
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: InkWell(
                child: Icon(Icons.delete, color: Color(0xff0036B0),),
                onTap: (){
                },),
            ),
            SizedBox(width: 30.0),
            Flexible(child: Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Text('Items that have been in spam for more than 30 days will be automatically deleted', style: TextStyle(
                color: Colors.grey[600]
              ),),
            )),
          ],
        ),
        SizedBox(height: 20.0),
        Padding(
          padding: const EdgeInsets.only(left: 72.0),
          child: InkWell(
            child: Text('Empty spam now', style: TextStyle(
              color: Color(0xff0036B0)
            ),
            ),
            onTap: (){

            },
          ),
        ),
        SizedBox(height: 20.0),
        Divider(
          height: 10,
          indent: 10,
          endIndent: 10,
          color: Colors.blueGrey,
        ),
        ListTile(
          onTap: () {
            Navigator.of(context).pushNamed('/mail-content');
          },
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          leading: CircleAvatar(
            radius: 22,
            backgroundColor: Colors.pinkAccent,
            child: Text(
              'A',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Asana',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xff00174A),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Notification of Task Asssignment',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff00174A),
                    fontSize: 14,
                  ),
                ),
              )
            ],
          ),
          subtitle: Text(
            'A task has been assigned to you by Ralph',
            style: TextStyle(fontSize: 12),
          ),
          isThreeLine: true,
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Aug 12',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff00174A),
                ),
              ),
              Icon(
                Icons.star_border,
                size: 17,
              )
            ],
          ),
        ),
        Divider(
          height: 10,
          indent: 60,
          endIndent: 10,
          color: Colors.blueGrey,
        ),
        ListTile(
          onTap: () {
            Navigator.of(context).pushNamed('/mail-content');
          },
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          leading: CircleAvatar(
            radius: 22,
            backgroundColor: Colors.blue,
            child: Text(
              'A',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Asana',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xff00174A),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Notification of Task Asssignment',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff00174A),
                    fontSize: 14,
                  ),
                ),
              )
            ],
          ),
          subtitle: Text(
            'A task has been assigned to you by Ralph',
            style: TextStyle(fontSize: 12),
          ),
          isThreeLine: true,
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Aug 12',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff00174A),
                ),
              ),
              Icon(
                Icons.star_border,
                size: 17,
              )
            ],
          ),
        ),
        Divider(
          height: 10,
          indent: 60,
          endIndent: 10,
          color: Colors.blueGrey,
        ),
        ListTile(
          onTap: () {
            Navigator.of(context).pushNamed('/mail-content');
          },
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          leading: CircleAvatar(
            radius: 22,
            backgroundColor: Colors.green,
            child: Text(
              'A',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Asana',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xff00174A),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Notification of Task Asssignment',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff00174A),
                    fontSize: 14,
                  ),
                ),
              )
            ],
          ),
          subtitle: Text(
            'A task has been assigned to you by Ralph',
            style: TextStyle(fontSize: 12),
          ),
          isThreeLine: true,
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Aug 12',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff00174A),
                ),
              ),
              Icon(
                Icons.star_border,
                size: 17,
              )
            ],
          ),
        ),
        Divider(
          height: 10,
          indent: 60,
          endIndent: 10,
          color: Colors.blueGrey,
        ),
        ListTile(
          onTap: () {
            Navigator.of(context).pushNamed('/mail-content');
          },
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          leading: CircleAvatar(
            radius: 22,
            backgroundColor: Colors.red,
            child: Text(
              'A',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Asana',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xff00174A),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Notification of Task Asssignment',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff00174A),
                    fontSize: 14,
                  ),
                ),
              )
            ],
          ),
          subtitle: Text(
            'A task has been assigned to you by Ralph',
            style: TextStyle(fontSize: 12),
          ),
          isThreeLine: true,
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Aug 12',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff00174A),
                ),
              ),
              Icon(
                Icons.star_border,
                size: 17,
              )
            ],
          ),
        ),
        Divider(
          height: 10,
          indent: 60,
          endIndent: 10,
          color: Colors.blueGrey,
        ),
        ListTile(
          onTap: () {
            Navigator.of(context).pushNamed('/mail-content');
          },
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          leading: CircleAvatar(
            radius: 22,
            backgroundColor: Colors.pinkAccent,
            child: Text(
              'A',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Asana',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xff00174A),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Notification of Task Asssignment',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff00174A),
                    fontSize: 14,
                  ),
                ),
              )
            ],
          ),
          subtitle: Text(
            'A task has been assigned to you by Ralph',
            style: TextStyle(fontSize: 12),
          ),
          isThreeLine: true,
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Aug 12',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff00174A),
                ),
              ),
              Icon(
                Icons.star_border,
                size: 17,
              )
            ],
          ),
        ),
        Divider(
          height: 10,
          indent: 60,
          endIndent: 10,
          color: Colors.blueGrey,
        ),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          leading: CircleAvatar(
            radius: 22,
            backgroundColor: Colors.pinkAccent,
            child: Text(
              'A',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Asana',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xff00174A),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Notification of Task Asssignment',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff00174A),
                    fontSize: 14,
                  ),
                ),
              )
            ],
          ),
          subtitle: Text(
            'A task has been assigned to you by Ralph',
            style: TextStyle(fontSize: 12),
          ),
          isThreeLine: true,
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Aug 12',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff00174A),
                ),
              ),
              Icon(
                Icons.star_border,
                size: 17,
              )
            ],
          ),
        ),
        Divider(
          height: 10,
          indent: 60,
          endIndent: 10,
          color: Colors.blueGrey,
        ),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          leading: CircleAvatar(
            radius: 22,
            backgroundColor: Colors.pinkAccent,
            child: Text(
              'A',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Asana',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xff00174A),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Notification of Task Asssignment',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff00174A),
                    fontSize: 14,
                  ),
                ),
              )
            ],
          ),
          subtitle: Text(
            'A task has been assigned to you by Ralph',
            style: TextStyle(fontSize: 12),
          ),


          isThreeLine: true,
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Aug 12',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff00174A),
                ),
              ),
              Icon(
                Icons.star_border,
                size: 17,
              )
            ],
          ),
        ),
        Divider(
          height: 10,
          indent: 60,
          endIndent: 10,
          color: Colors.blueGrey,
        ),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          leading: CircleAvatar(
            radius: 22,
            backgroundColor: Colors.pinkAccent,
            child: Text(
              'A',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Asana',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xff00174A),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Notification of Task Asssignment',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff00174A),
                    fontSize: 14,
                  ),
                ),
              )
            ],
          ),
          subtitle: Text(
            'A task has been assigned to you by Ralph',
            style: TextStyle(fontSize: 12),
          ),
          isThreeLine: true,
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Aug 12',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff00174A),
                ),
              ),
              Icon(
                Icons.star_border,
                size: 17,
              )
            ],
          ),
        ),
        Divider(
          height: 10,
          indent: 60,
          endIndent: 10,
          color: Colors.blueGrey,
        ),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
          leading: CircleAvatar(
            radius: 22,
            backgroundColor: Colors.pinkAccent,
            child: Text(
              'A',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  'Asana',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xff00174A),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Notification of Task Asssignment',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Color(0xff00174A),
                    fontSize: 14,
                  ),
                ),
              )
            ],
          ),
          subtitle: Text(
            'A task has been assigned to you by Ralph',
            style: TextStyle(fontSize: 12),
          ),
          isThreeLine: true,
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Aug 12',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff00174A),
                ),
              ),
              Icon(
                Icons.star_border,
                size: 17,
              )
            ],
          ),
        ),
        Divider(
          height: 10,
          indent: 60,
          endIndent: 10,
          color: Colors.blueGrey,
        ),
      ],
    );
  }
}

const List<InboxItem> inboxItems = const <InboxItem>[
  const InboxItem(
    date: 'Aug 12',
    subject: 'Notification of Result',
    from: 'Chris Sevjilla',
    message: 'Thank you for your support .....',
  )
];

class InboxItem {
  final String from;
  final String subject;
  final String message;
  final String date;

  const InboxItem({this.from, this.subject, this.message, this.date});
}
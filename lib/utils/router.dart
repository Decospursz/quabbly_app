import 'package:flutter/material.dart';
import 'package:quabbly_app/ui/screens/dashboard_screen.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => DashboardScreen());
    }
  }
}
//TODO change router implementation to this

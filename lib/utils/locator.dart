import 'package:get_it/get_it.dart';
import 'package:quabbly_app/core/services/auth_service.dart';
import 'package:quabbly_app/core/viewmodels/login/login-model.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerSingleton<AuthService>(new AuthService());

  locator.registerFactory(() => LoginModel());
}

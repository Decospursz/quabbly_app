class BaseService {
  static const _baseURL = 'https://p-user-api-dev.quabbly.com/v1';

  static get baseURL => _baseURL;
}
